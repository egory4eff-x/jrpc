package storages

import (
	"gitlab.com/egory4eff-x/jrpc-users/internal/db/adapter"
	"gitlab.com/egory4eff-x/jrpc-users/internal/infrastructure/cache"
	vstorage "gitlab.com/egory4eff-x/jrpc-users/internal/modules/auth/storage"
	ustorage "gitlab.com/egory4eff-x/jrpc-users/internal/modules/user/storage"
)

type Storages struct {
	User   ustorage.Userer
	Verify vstorage.Verifier
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		User:   ustorage.NewUserStorage(sqlAdapter, cache),
		Verify: vstorage.NewEmailVerify(sqlAdapter),
	}
}
