package storage

import (
	"context"
	"gitlab.com/egory4eff-x/jrpc-users/internal/models"
)

type Verifier interface {
	GetByEmail(ctx context.Context, email, hash string) (models.EmailVerifyDTO, error)
	GetByUserID(ctx context.Context, userID int) (models.EmailVerifyDTO, error)
	Verify(ctx context.Context, userID int) error
	VerifyEmail(ctx context.Context, email, hash string) error
	Create(ctx context.Context, email, hash string, userID int) error
}
