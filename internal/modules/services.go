package modules

import (
	"gitlab.com/egory4eff-x/jrpc-users/internal/infrastructure/component"
	aservice "gitlab.com/egory4eff-x/jrpc-users/internal/modules/auth/service"
	uservice "gitlab.com/egory4eff-x/jrpc-users/internal/modules/user/service"
	"gitlab.com/egory4eff-x/jrpc-users/internal/storages"
)

type Services struct {
	User          uservice.Userer
	Auth          aservice.Auther
	UserClientRPC uservice.Userer
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	userService := uservice.NewUserService(storages.User, components.Logger)
	return &Services{
		User: userService,
		Auth: aservice.NewAuth(userService, storages.Verify, components),
	}
}
